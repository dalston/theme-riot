//SEARCH ICON 

ThemePress.toFinalize(function ($) {
	$(".view-mode .brikit-header-container .targeted-search-field-query,").removeAttr("placeholder");
	
	var focusInput = true;
	$('.view-mode .search-glass').click(function() {
       $(".targeted-search-field-results").removeClass("hidden");
	   $('.aui-quicksearch').addClass('search-expanded');
	   $('.search-glass').addClass('search-glass-expanded');
	   $(".brikit-header-container .targeted-search-field-query").attr("placeholder", "Search");
	   $(".brikit-header-container .targeted-search-field-query").css('width', '180px');
		if (focusInput) {
			$('.brikit-header-container .targeted-search-field-query').focus();
		} else {
			$('.brikit-header-container .targeted-search-field-query').blur();
		}
		focusInput = !focusInput;
	});
	$('.view-mode .brikit-header-container .targeted-search-field-query').on("focusout", function(){
	  setTimeout(function(){          
		  $(".brikit-header-container .targeted-search-field-query").removeAttr("placeholder");
		  $(".brikit-header-container .targeted-search-field-query").css('width', '0');
		  $('.search-glass').removeClass('search-glass-expanded');
		  $(".aui-quicksearch").removeClass("search-expanded");
	      $(".recently-viewed-dropdown").addClass("hidden");
	      focusInput = !focusInput;
   	  }, 250);
	});
    $(window).blur(function(){
      $(".brikit-header-container .targeted-search-field-query").blur(); 
      $(".brikit-header-container .targeted-search-field-results").addClass("hidden");
      focusInput = !focusInput;

    });

    
});